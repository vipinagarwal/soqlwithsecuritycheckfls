  @AuraEnabled
    public static List<ActivityWrapper> getCheckInLogs(String contactId){
        List<ActivityWrapper> actList = new List<ActivityWrapper>();
        if(contactId !=null){
            for(Activity_Log__c act : [SELECT Id,Visitation_Date_Time__c,Visiting_Address__c,Visitor__c FROM Activity_Log__c WHERE Visitor__c =: contactId WITH SECURITY_ENFORCED ORDER BY Visitation_Date_Time__c DESC NULLS LAST]){
                ActivityWrapper ac = new ActivityWrapper();
                ac.activityLogId = act.Id;
                string meridiem = act.Visitation_Date_Time__c.hour() < 12 ? 'AM' : 'PM';
                ac.visitationDateTime =  act.Visitation_Date_Time__c.month() + '-' + act.Visitation_Date_Time__c.day() + '-' + act.Visitation_Date_Time__c.year() +' '+act.Visitation_Date_Time__c.hour() + ':' + act.Visitation_Date_Time__c.minute()+ ':' +  act.Visitation_Date_Time__c.second() +' '+ meridiem;
               // ac.VisitationDateTime = act.Visitation_Date_Time__c.format('MMMMM dd, yyyy hh:mm:ss a');
                ac.address = act.Visiting_Address__c;
                actList.add(ac);
            }
        }
        return actList;
    }